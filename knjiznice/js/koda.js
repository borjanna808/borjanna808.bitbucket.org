
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}
console.log('testtt');
$(document).ready(function(){
    console.log('test');
    // TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
    $('#submit').click(function(){
        console.log('test');
        var response = $.ajax({
            url: baseUrl + '/ehr',
            type: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(username + ':' + password)
            },
            dataType: 'json',
            success: function(){
                console.log(response);
                $('#ehrId').val(response.responseJSON.ehrId);
                var data = {partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: response.responseJSON.ehrId
                    }
                ]
                };
                data.firstNames = $('#name').val();
                data.lastNames = $('#surname').val();
                var response2 = $.ajax({
                    url: baseUrl + '/demographics/party',
                    type: 'POST',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + btoa(username + ':' + password)
                    },
                    dataType: 'json'
                });
            }
        });
        
    });
});


